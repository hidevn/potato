import config
import itertools
import copy

class Setting():
    available_universe = config.AVAILABLE_UNIVERSE
    available_region = config.AVAILABLE_REGION
    available_delay = config.AVAILABLE_DELAY
    available_neut = config.AVAILABLE_NEUT
    available_decay = config.AVAILABLE_DECAY
    available_weight = config.AVAILABLE_WEIGHT
    available_lookback = config.AVAILABLE_LOOKBACK
    
    def __init__(self, setting = None):
        self.setting = config.DEFAULT_POST_SETTING
        if setting == None:
            self.load_setting(config.DEFAULT_SETTING)
        else:
            self.load_setting(setting)
    
    def set_uni(self, uni):
        if uni in self.available_universe:
            self.setting['univid'] = uni
        else:
            raise Exception("Universe must be " + str(self.available_universe))
    
    def set_region(self, reg):
        if reg in self.available_region:
            self.setting['region'] = reg
        else:
            raise Exception("Region must be " + str(self.available_region))
        
    def set_delay(self, de):
        if de in self.available_delay:
            self.setting['delay'] = de
        else:
            raise Exception("Delay must be " + str(self.available_delay))
        
    def set_neut(self, neut):
        if neut in self.available_neut:
            self.setting['opneut'] = neut
        else:
            raise Exception("Neutralization must be " + str(self.available_neut))

    def set_weight(self, w):
        self.setting['optrunc'] = float(w)
        
    def set_lookback(self, d):
        self.setting['backdays'] = int(d)
        
    def set_decay(self, d):
        self.setting['decay'] = int(d)

    def set_alpha(self, alpha):
        self.setting['code'] = alpha
        
    
    def load_setting(self, setting):
        self.set_region(setting['region'])
        self.set_uni(setting['universe'])
        self.set_delay(setting['delay'])
        self.set_decay(setting['decay'])
        self.set_weight(setting['max_stock_weight'])
        self.set_neut(setting['neutralization'])
        self.set_lookback(setting['lookback_days'])
        
    def get(self):
        return self.setting
    
    @classmethod
    def all_setting_gen(cls):
        available_settings = [cls.available_region, cls.available_universe, cls.available_delay, cls.available_decay,
                              cls.available_weight, cls.available_neut, cls.available_lookback]
        list_setting = list(itertools.product(*available_settings))
        mapped_setting = [{'region': x[0], 'universe': x[1], 'delay': x[2], 
                           'decay': x[3], 'max_stock_weight': x[4], 'neutralization': x[5],
                           'lookback_days': x[6]} for x in list_setting]
        for setting in mapped_setting:
            s = Setting()
            s.load_setting(setting)
            yield s
    
    @classmethod
    def all_setting(cls):
        available_settings = [cls.available_region, cls.available_universe, cls.available_delay, cls.available_decay,
                              cls.available_weight, cls.available_neut, cls.available_lookback]
        list_setting = list(itertools.product(*available_settings))
        mapped_setting = [{'region': x[0], 'universe': x[1], 'delay': x[2], 
                           'decay': x[3], 'max_stock_weight': x[4], 'neutralization': x[5],
                           'lookback_days': x[6]} for x in list_setting]
        return [copy.deepcopy(Setting(setting)) for setting in mapped_setting]
        
if __name__ == '__main__':
    r = Setting.all_setting()