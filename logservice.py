import sys

class LogService():
    def __init__(self, log = True, filename = None):
        self.log_opt = log
        if (log == True or log == 'both') and filename != None:
            self.log_file = open(filename, 'a', encoding='utf-8')
    
    def log(self, log_text):
        if self.log_opt == True:
            self.log_file.write('\n')
            self.log_file.write(log_text)
            self.log_file.flush()
        elif self.log_opt == False:
            print(log_text, flush=True)
            
        elif self.log_opt == 'both':
            self.log_file.write('\n')
            self.log_file.write(log_text)
            self.log_file.flush()
            print(log_text)
            sys.stdout.flush()
            
            
            
    def print_dict(self, js, prop = None, remove = None):
        if prop == None:
            if remove == None:
                return str(js)[1:-1]
            else:
                return str(js)[1:-1].replace(remove, '')
        else:
            if prop in js:
                if remove == None:
                    return str(js[prop])
                else:
                    return str(js[prop]).replace(remove, '')
            else:
                return ''
        
    def alpha_log(self, alpha, setting, grade, summary):
        self.log('------------------------------')
        self.log('Current alpha: ' + alpha)
        self.log('Current setting: ')
        self.setting_log(setting)
        self.log('Grade: ' + self.print_dict(grade.json(), 'result'))
        if not 'result' in summary.json():
            self.log('Some error happened!!!!')
            return False
        else:
            s = summary.json()['result'][-1]
            del s['Year']
            del s['YearId']
            del s['BookSize']
            self.log(self.print_dict(s, remove="'"))
            if s['Sharpe'] >= 1.25:
                self.log('THE KING HAS COME!')
            return True
        
    def error_log(self, alpha, setting, error):
        self.log('------------------------------')
        self.log('Current alpha: ' + alpha)
        self.log('Current setting: ')
        self.setting_log(setting)
        self.log('Encountered Error:')
        self.log(self.print_dict(error.json()))
        return True
    
    def simulate_error_log(self, alpha, setting, simulate_request):
        self.log('------------------------------')
        self.log('Current alpha: ' + alpha)
        self.log('Current setting: ')
        self.setting_log(setting)
        self.log('Encountered Simulate Error:')
        #self.print_dict(simulate_request.json())
        self.log(simulate_request.json()['error'])
        return True
          
    def alpha_error_log(self, alpha, setting, error_req):
        self.log('------------------------------')
        self.log('Current alpha: ' + alpha)
        self.log('Current setting: ')
        self.setting_log(setting)
        self.log('Encountered Alpha Error:')
        self.print_dict(error_req.json())
        return True
    
    def setting_log(self, setting):
        self.log('Region: ' + setting['region'] + ', Universe: ' + setting['univid'] +
                 ', Delay: ' + str(setting['delay']) + ', Decay: ' + str(setting['decay']) +
                ', Weight: ' + str(setting['optrunc']) + ', Neutralization: ' + str(setting['opneut']) +
                ', Backdays: ' + str(setting['backdays']))
                
    def str_append(self, string, content):
        string += content + '\n'
        return string
    
    def process_alpha_log(self, alpha, setting, grade, summary):
        result = ''
        self.str_append(result, '------------------------------')
        self.str_append(result, 'Current alpha: ' + alpha)
        self.str_append(result, 'Current setting: ')
        #self.setting_log(setting)
        self.str_append(result, 'Grade: ' + self.print_dict(grade.json(), 'result'))
        if not 'result' in summary.json():
            self.str_append(result, 'Some error happened!!!!')
            return result
        else:
            s = summary.json()['result'][-1]
            del s['Year']
            del s['YearId']
            del s['BookSize']
            self.str_append(result, self.print_dict(s, remove="'"))
            if s['Sharpe'] >= 2 and s['Fitness']>1:
                self.str_append(result, 'Sharpe ngon vkl!')
            elif s['Sharpe'] >= 1.25 and s['Fitness']>1:
                self.str_append(result, 'Sharpe ngon!')
            return result

    def done_reporter(self, alpha):
        self.log('------------------------------')
        self.log('Done ^_^')
        self.log('------------------------------')