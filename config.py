# the config reader
import json

with open('config.json') as j:
    config = json.load(j)
    
try:
    domain = config['endpoints']['domain']
    LOGIN_ENDPOINT = domain + config['endpoints']['login']
    USER_ENDPOINT = domain + config['endpoints']['user']
    USERINFO_ENDPOINT = domain + config['endpoints']['userinfo']
    SIMULATE_ENDPOINT = domain + config['endpoints']['simulate']
    SSETTING_ENDPOINT = domain + config['endpoints']['simulate_setting']
    JOBDETAILS_ENDPOINT = domain + config['endpoints']['job_detail']
    JOBPROGRESS_ENDPOINT = domain + config['endpoints']['job_progress']
    PNL_ENDPOINT = domain + config['endpoints']['pnl']
    ERROR_ENDPOINT = domain + config['endpoints']['error']
    GRADE_ENDPOINT = domain + config['endpoints']['grade']
    SUMMARY_ENDPOINT = domain + config['endpoints']['summary']
    
    
    EMAIL = config['login_info']['email']
    PASSWORD = config['login_info']['password']
    
    DEFAULT_SETTING = config['default_setting']
    DEFAULT_POST_SETTING = config['default_post_setting']
    AVAILABLE_UNIVERSE = config['available_settings']['universe']
    AVAILABLE_REGION = config['available_settings']['region']
    AVAILABLE_DELAY = config['available_settings']['delay']
    AVAILABLE_NEUT = config['available_settings']['neutralization']
    AVAILABLE_DECAY = config['available_settings']['decay']
    AVAILABLE_WEIGHT = config['available_settings']['max_stock_weight']
    AVAILABLE_LOOKBACK = config['available_settings']['lookback_days']
    

    SINGLE_ALPHA_MOD = config['single_alpha_mod']
    COMBO_ALPHA_MOD = config['combo_alpha_mod']

    FUNDAMENTAL = config['fundamental']
    
    NUMBER_OF_PROCESS = config['num_process']
    LOGFILE = config['logfile']
    
except KeyError as e:
    raise Exception("Missing key: " + e.args[0])