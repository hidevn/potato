#FAWS

import requests
import time
import config as config
from matplotlib import pyplot as plt
from datetime import datetime
import numpy as np
from logservice import LogService
from setting import Setting
from multiprocessing import Pool, Process, Manager
from threading import Lock
import sys
import itertools

#pnl = None

class Session():
    
    def __init__(self, mutex, log = True, plot = False):
        self.num_process = int(config.NUMBER_OF_PROCESS)
        self.pool = Pool(self.num_process)
        self.session = requests.Session()
        self.session.headers = {'User-Agent': 'Mozilla/5.0',
                                'Host': 'www.worldquantvrc.com',
                                'Origin': 'https://www.worldquantvrc.com'}
        
        self.log_service = LogService(log=log, filename=config.LOGFILE)
        self.login(config.EMAIL, config.PASSWORD)
        self.setting = Setting()
        self.log_opt = log
        self.plot_opt = plot
        self.mutex = mutex
        self.funda = False
   
    
    def login(self, email, password):
        self.log_service.log('------------------------------------------------------------')
        self.log_service.log('Logging In')
        payload = {'EmailAddress': email, 
                   'Password': password,
                   'next': '',
                   'g-recaptcha-response': ''}
        try:
            s = self.session.post(config.LOGIN_ENDPOINT, data = payload)
            if 'status' in s.json() and s.json()['status'] == False:
                self.log_service.log('WRONG USERNAME AND PASSWORD DUDE!')
                return False
            elif 'status' in s.json() and s.json()['status'] == True:
                self.log_service.log('Starting a session with email: ' + email)
                return True
        except requests.exceptions.ConnectionError as e:
            self.log_service.log('Connection error: Can\'t login')

    # Sml all alphas in array with all setting        
    def lazy_simulate_async(self, alphas):
        for alpha in alphas:
            self.multi_setting_simulate_async(alpha)
            
    # Sml all alphas in file with all setting
    def lazy_file_simulate_async(self):
        alphas_file = open('alpha.txt', 'r')
        alphas = alphas_file.read()
        alphas = alphas.split('\n--\n')
        alphas_file.close()
        for alpha in alphas:
            self.multi_setting_simulate_async(alpha)
    
    # Sml all alphas generated in file with all setting
    def lazy_file_gen_async(self):
        generated_alphas = []
        alphas_file = open('alpha.txt', 'r')
        alphas = alphas_file.read()
        alphas = alphas.split('\n--\n')
        alphas_file.close()
        for alpha in alphas:
            new_alphas = [string.replace('#####', alpha) for string in config.SINGLE_ALPHA_MOD]
            generated_alphas.extend(new_alphas)
        for alpha in generated_alphas:
            self.multi_setting_simulate_async(alpha)
            
    def file_simulate_async(self):
        alphas_file = open('alpha.txt', 'r')
        alphas = alphas_file.read()
        alphas = alphas.split('\n--\n')
        alphas_file.close()
        self.pool.map_async(self.simulate, alphas, callback=self.log_service.done_reporter)
        
    def file_simulate_all_setting_async(self):
        alphas_file = open('alpha.txt', 'r')
        alphas = alphas_file.read()
        alphas = alphas.split('\n--\n')
        alphas_file.close()
        settings = Setting.all_setting()
        it = list(itertools.product(alphas, settings))
        self.pool.starmap_async(self.simulate, it, callback=self.log_service.done_reporter)
        
        
    def alphas_simulate_async(self, alphas):
        self.pool.map_async(self.simulate, alphas, callback=self.log_service.done_reporter)
        
    def multi_setting_simulate_async(self, alpha):
        settings = Setting.all_setting()
        it = [(alpha, setting) for setting in settings]
        self.pool.starmap_async(self.simulate, it, callback=self.log_service.done_reporter)
    
    def simulate_async(self, alpha, setting = None):
        it = [(alpha, setting)]
        self.pool.starmap_async(self.simulate, it, callback=self.log_service.done_reporter)
        
    def gen_alpha_async(self, alpha):
        generated_alphas = [string.replace('#####', alpha) for string in config.SINGLE_ALPHA_MOD]
        self.alphas_simulate_async(generated_alphas)
        
    def gen_alpha_all_setting_async(self, alpha):
        generated_alphas = [string.replace('#####', alpha) for string in config.SINGLE_ALPHA_MOD]
        settings = Setting.all_setting()
        it = list(itertools.product(generated_alphas, settings))
        self.pool.starmap_async(self.simulate, it, callback=self.log_service.done_reporter)
        
    def gen_alphas_async(self):
        generated_alphas = []
        alphas_file = open('alpha.txt', 'r')
        alphas = alphas_file.read()
        alphas = alphas.split('\n--\n')
        alphas_file.close()
        for alpha in alphas:
            new_alphas = [string.replace('#####', alpha) for string in config.SINGLE_ALPHA_MOD]
            generated_alphas.extend(new_alphas)
        self.alphas_simulate_async(generated_alphas)
        
    def gen_alphas_all_setting_async(self):
        generated_alphas = []
        alphas_file = open('alpha.txt', 'r')
        alphas = alphas_file.read()
        alphas = alphas.split('\n--\n')
        alphas_file.close()
        for alpha in alphas:
            new_alphas = [string.replace('#####', alpha) for string in config.SINGLE_ALPHA_MOD]
            generated_alphas.extend(new_alphas)
        settings = Setting.all_setting()
        it = list(itertools.product(generated_alphas, settings))
        self.pool.starmap_async(self.simulate, it, callback=self.log_service.done_reporter)
    
    
    def combo_alpha_async(self, alpha1, alpha2):
        alphas = []
        for t in config.COMBO_ALPHA_MOD:
            alpha = t.replace('#####', 'scale(' + alpha1 + ')').replace('$$$$$', 'scale(' + alpha2 + ')')
            alphas.append(alpha)
        self.pool.map_async(self.simulate, alphas, callback=self.log_service.done_reporter)
            
            
    def combo_alpha_all_setting_async(self, alpha1, alpha2):
        alphas = []
        for t in config.COMBO_ALPHA_MOD:
            #alpha = t.replace('#####', 'scale(' + alpha1 + ')').replace('$$$$$', 'scale(' + alpha2 + ')')
            alpha = t.replace('#####', alpha1).replace('$$$$$',alpha2)
            alphas.append(alpha)
        settings = Setting.all_setting()
        it = list(itertools.product(alphas, settings))
        self.pool.starmap_async(self.simulate, it, callback=self.log_service.done_reporter)

    def fundamental_list(self):
        fun_len = len(config.FUNDAMENTAL)
        alpha_list = []
        #checkpoint = open('checkpoint.txt', mode='a+')
        self.funda = True
        file = open('log_fun.txt', 'r')
        content = file.read()
        line_split = content.split('\n')
        ran_alpha = []
        for line in line_split:
            if "Current alpha" in line:
                ran_alpha.append(line[15:])
        def callpack(alpha):
            self.funda = False
        for i in range(0, fun_len):
            for j in range(0, fun_len):
                if i != j:
                    alpha_list.append(str(config.FUNDAMENTAL[i]) + "/" + str(config.FUNDAMENTAL[j]))
                    alpha_list.append('rank(' + str(config.FUNDAMENTAL[i]) + "/" + str(config.FUNDAMENTAL[j])+ ')')
        must_run_list = [alpha for alpha in alpha_list if alpha not in ran_alpha]
        self.pool.map_async(self.simulate, must_run_list, callback=self.log_service.done_reporter)
                
        
    # setting == None: use self.setting
    # setting != None: use setting in args
    def simulate(self, alpha, setting = None):
        if alpha == "":
            alphas_file = open('alpha.txt', 'r')
            alphas = alphas_file.read()
            alphas = alphas.split('\n--\n')
            alpha = alphas[0]
            alphas_file.close()
            
        if setting == None:
            setting = self.setting
        setting.set_alpha(alpha)
        setting = setting.get()
        payload = {'args': '[' + str(setting).replace(' ', '').replace('\'', '"').replace('"null"', 'null') + ']'}
        try:
            simulate_request = self.session.post(config.SIMULATE_ENDPOINT, data = payload)
            while simulate_request.ok and simulate_request.status_code == 200 and simulate_request.json()['error'] != None and 'all' in simulate_request.json()['error'] and 'limit of concurrent simulations' in simulate_request.json()['error']['all']:
                time.sleep(1)
                simulate_request = self.session.post(config.SIMULATE_ENDPOINT, data = payload)   

            if simulate_request.ok and simulate_request.status_code == 200:
                if simulate_request.json()['error'] == None:
                    job_id = simulate_request.json()['result'][0]
                    # Sua loi request chua xong?? Hay la lam sao????
                    while True:
                        if self.plot_opt:
                            pnl_request = self.session.post(config.PNL_ENDPOINT + str(job_id))
                        progress_request = self.session.post(config.JOBPROGRESS_ENDPOINT + str(job_id))
                        if progress_request.json() != 'DONE' and progress_request.json() != 'ERROR':
                            #print(progress_request.json())
                            time.sleep(0.5)  
                            continue
                        error_request = self.session.post(config.ERROR_ENDPOINT + str(job_id))
                        grade_request = self.session.post(config.GRADE_ENDPOINT + str(job_id))
                        summary_request = self.session.post(config.SUMMARY_ENDPOINT + str(job_id))
                        if progress_request.json() == 'ERROR':
                            break
                        if len(summary_request.json()['result']) != 0:
                            # WTF, why result == 0?
                            break
                        #print(progress_request.json())
                        #time.sleep(1)                    
                        
                    if error_request.json() == None:
                        self.mutex.acquire()
                        self.log_service.alpha_log(alpha, setting, grade_request, summary_request)
                        self.mutex.release()
                            
                        #if self.plot_opt:
                        #    self.plot(pnl_request.json()['result'])
                    else:
                        self.mutex.acquire()
                        self.log_service.error_log(alpha, setting, error_request)
                        self.mutex.release()
                else:
                    self.mutex.acquire()
                    self.log_service.simulate_error_log(alpha, setting, simulate_request)
                    self.mutex.release()
            else:
                self.mutex.acquire()
                self.log_service.log('------------------------------')
                self.log_service.log('Connection error: ' + str(simulate_request.status_code))
                self.mutex.release()
        except requests.exceptions.ConnectionError as e:
            self.mutex.acquire()
            self.log_service.log('------------------------------')
            self.log_service.log('Connection error')
            self.log_service.log(str(e))
            self.mutex.release()
            
    def stop(self):
        self.pool.terminate()
        self.pool = Pool(self.num_process)
        self.log_service.log('------------------------------')
        self.log_service.log('Stopped')
        self.log_service.log('------------------------------')
        self.funda = False
        try:
            self.mutex.release()
        except RuntimeError:
            pass
    
        
    def __getstate__(self):
        self_dict = self.__dict__.copy()
        del self_dict['pool']
        return self_dict

    def __setstate__(self, state):
        self.__dict__.update(state)
            
    
if __name__ == '__main__':
    m = Manager()
    mutex = m.Lock()
    
    s = Session(mutex, False, False)
    s.pool.map(s.simulate, ['asset'])    
