from tkinter import Tk, Text, TOP, BOTH, X, Y, N, S, E, W, LEFT, StringVar, OptionMenu, ttk, Canvas, PhotoImage, messagebox
from tkinter.ttk import Frame, Button, Style, Entry, Label, Combobox
import config as config
import consession
from multiprocessing import Manager

#ttk.Style()
#style.configure("vista")

class Interface(Frame):
    def __init__(self, parent):
        Frame.__init__(self, parent)
  
        self.parent = parent
        self.initUI()
        self.sess = None
  
    def initUI(self):
        self.manager = Manager()
        self.lock = self.manager.Lock()
        self.sess = consession.Session(self.lock, log = False, plot = False)
        global stop_flag
        global run_flag
        self.parent.title("FAGS")
        self.style = Style()
        self.style.theme_use("vista")
        self.pack(fill=BOTH, expand=True)
        self.columnconfigure(0, pad=3, weight=1)
        self.rowconfigure(0, pad=3, weight=1)
        
        frame1 = Frame(self)
        frame1.grid(row = 0, column = 0, padx = 20, pady = 10, sticky=N+S+E+W)
        
        
        alpha_box = Text(frame1, height = 5, wrap='none')
        alpha_box.pack(fill=X, pady = (0, 20))
        
        alpha_box_2 = Text(frame1, height = 5, wrap='none')
        alpha_box_2.pack(fill=X, pady=(0, 20))
        
        def get_alpha():
            inputValue=alpha_box.get("1.0","end-1c")
            return inputValue

        def get_alpha_2():
            inputValue=alpha_box_2.get("1.0", "end-1c")
            return inputValue
        
        left_child = Frame(frame1)
        left_child.pack(fill=X)
        left_child.columnconfigure(0, weight=1)
        left_child.columnconfigure(1, weight=3)
        
        label_uni = Label(left_child, text="Universe", width=10)
        label_uni.grid(row=0, column=0, pady = 5, sticky=N+S+E+W)         
        uni_drop = Combobox(left_child, state="readonly", values=config.AVAILABLE_UNIVERSE)
        uni_drop.set(config.DEFAULT_SETTING["universe"])
        uni_drop.grid(row=0, column=1, pady = 5, sticky=N+S+E+W)  
        
        def get_universe():
            inputValue=uni_drop.get()
            return inputValue


        label_decay = Label(left_child, text="Decay", width=10)
        label_decay.grid(row=1, column=0, pady = 5, sticky=N+S+E+W) 
        decay_box = Entry(left_child)
        decay_box.grid(row=1, column=1, pady = 5, sticky=N+S+E+W)
        decay_box.insert(0, config.DEFAULT_SETTING["decay"])
        
        def get_decay():
            inputValue = int(decay_box.get())
            return inputValue

        label_delay = Label(left_child, text="Delay", width=10)
        label_delay.grid(row=2, column=0, pady = 5, sticky=N+S+E+W)         
        delay_drop = Combobox(left_child, state="readonly", values=config.AVAILABLE_DELAY)
        delay_drop.set(config.DEFAULT_SETTING["delay"])
        delay_drop.grid(row=2, column=1, pady = 5, sticky=N+S+E+W)  
        
        def get_delay():
            inputValue=delay_drop.get()
            return inputValue
        
        
        label_weight = Label(left_child, text="Weight", width=10)
        label_weight.grid(row=3, column=0, pady = 5, sticky=N+S+E+W) 
        weight_box = Entry(left_child)
        weight_box.grid(row=3, column=1, pady = 5, sticky=N+S+E+W) 
        weight_box.insert(0, config.DEFAULT_SETTING["max_stock_weight"])
        
        def get_weight():
            inputValue = float(weight_box.get())
            return inputValue
        
        label_neu = Label(left_child, text="Neutralization", width=10)
        label_neu.grid(row=4, column=0, pady = 5, sticky=N+S+E+W)         
        neu_drop = Combobox(left_child, state="readonly", values=config.AVAILABLE_NEUT)
        neu_drop.set(config.DEFAULT_SETTING["neutralization"])
        neu_drop.grid(row=4, column=1, pady = 5, sticky=N+S+E+W)  
        
        def get_neut():
            inputValue = neu_drop.get()
            return inputValue
        
        label_type = Label(left_child, text="Setting", width=10)
        label_type.grid(row=5, column=0, pady = 5, sticky=N+S+E+W)         
        type_drop = Combobox(left_child, state="readonly", values=['One', 'All'])
        type_drop.set('One')
        type_drop.grid(row=5, column=1, pady = 5, sticky=N+S+E+W)  
        
        def get_type():
            inputValue = type_drop.get()
            return inputValue
        
        '''
        label_lookback = Label(left_child, text="Lookback", width=10)
        label_lookback.grid(row=4, column=0, pady = 5, sticky=N+S+E+W) 
        lookback_box = Entry(left_child)
        lookback_box.grid(row=4, column=1, pady = 5, sticky=N+S+E+W) 
        '''
        
        nested_child = Frame(frame1)
        nested_child.pack(fill=X, pady = (30, 0))
        nested_child.columnconfigure(0, weight=1)
        nested_child.columnconfigure(1, weight=1)
        nested_child.columnconfigure(2, weight=1)
        nested_child.columnconfigure(3, weight=1)
        nested_child.columnconfigure(4, weight=1)
        nested_child.columnconfigure(5, weight=1)
        nested_child.columnconfigure(6, weight=1)
        #run_flag = True
        #stop_flag = False
        sess = self.sess
        
        def simulate_single_alpha():
            global run_flag
            if get_alpha() == '':
                messagebox.showerror("Error", "Đcm chưa nhập alpha")
                return
            if run_flag == True:
                run_flag = False
                alpha = get_alpha()
                uni = get_universe()
                decay = get_decay()
                weight = get_weight()
                delay = get_delay()
                neut = get_neut()
                typ = get_type()
                sess.setting.set_uni(uni)
                sess.setting.set_decay(decay)
                sess.setting.set_weight(weight)
                sess.setting.set_neut(neut)
                sess.setting.set_delay(delay)
                if typ == 'One':
                    sess.simulate_async(alpha, setting = None)
                else:
                    sess.multi_setting_simulate_async(alpha)
                run_flag = True  
            else:
                messagebox.showerror("Error", "Đang chạy dở")
                return
        
        sim_button = Button(nested_child, text="Simulate", command=lambda: simulate_single_alpha())
        sim_button.grid(row=0, column=0)
        
        def simulate_file():
            global run_flag
            if run_flag == True:
                run_flag = False
                uni = get_universe()
                decay = get_decay()
                weight = get_weight()
                neut = get_neut()
                typ = get_type()
                delay = get_delay()
                sess.setting.set_uni(uni)
                sess.setting.set_decay(decay)
                sess.setting.set_weight(weight)
                sess.setting.set_neut(neut)
                sess.setting.set_delay(delay)
                if typ == 'One':
                    sess.file_simulate_async()
                else:
                    sess.file_simulate_all_setting_async()
                run_flag = True
            else:
                messagebox.showerror("Error", "Đang chạy dở")
                return
                            
        sim_all_button = Button(nested_child, text="Simulate từ file", command=lambda: simulate_file())
        sim_all_button.grid(row=0, column=1)    
        
        def gen_alpha():
            global run_flag
            if get_alpha() == '':
                messagebox.showerror("Error", "Đcm chưa nhập alpha")
                return
            if run_flag == True:
                run_flag = False
                alpha = get_alpha()
                uni = get_universe()
                decay = get_decay()
                weight = get_weight()
                neut = get_neut()
                typ = get_type()
                delay = get_delay()
                sess.setting.set_uni(uni)
                sess.setting.set_decay(decay)
                sess.setting.set_weight(weight)
                sess.setting.set_neut(neut)
                sess.setting.set_delay(delay)
                if typ == 'One':
                    sess.gen_alpha_async(alpha)
                else:
                    sess.gen_alpha_all_setting_async(alpha)
                run_flag = True
            else:
                messagebox.showerror("Error", "Đang chạy dở")
                return
            
        gen_alpha_button = Button(nested_child, text="Gen Alpha", command=lambda: gen_alpha())
        gen_alpha_button.grid(row=0, column=2)
        
        def gen_alpha_file():
            global run_flag
            if get_alpha() == '':
                messagebox.showerror("Error", "Đcm chưa nhập alpha")
                return
            if run_flag == True:
                run_flag = False
                uni = get_universe()
                decay = get_decay()
                weight = get_weight()
                neut = get_neut()
                typ = get_type()
                delay = get_delay()
                sess.setting.set_uni(uni)
                sess.setting.set_decay(decay)
                sess.setting.set_weight(weight)
                sess.setting.set_neut(neut)
                sess.setting.set_delay(delay)
                if typ == 'One':
                    sess.gen_alphas_async()
                else:
                    sess.gen_alphas_all_setting_async()
                run_flag = True
            else:
                messagebox.showerror("Error", "Đang chạy dở")
                return
            
        gen_alphas_button = Button(nested_child, text="Gen Alpha từ file", command=lambda: gen_alpha_file())
        gen_alphas_button.grid(row=0, column=3)
        
        def combo_alpha():
            global run_flag
            if get_alpha() == '' or get_alpha_2() == '':
                messagebox.showerror("Error", "Đcm chưa nhập alpha")
                return
            if run_flag == True:
                run_flag = False
                alpha = get_alpha()
                alpha2 = get_alpha_2()
                uni = get_universe()
                decay = get_decay()
                weight = get_weight()
                neut = get_neut()
                typ = get_type()
                delay = get_delay()
                sess.setting.set_uni(uni)
                sess.setting.set_decay(decay)
                sess.setting.set_weight(weight)
                sess.setting.set_neut(neut)
                sess.setting.set_delay(delay)
                if typ == 'One':
                    sess.combo_alpha_async(alpha1=alpha, alpha2=alpha2)
                else:
                    sess.combo_alpha_all_setting_async(alpha1=alpha, alpha2=alpha2)
                run_flag = True
            else:
                messagebox.showerror("Error", "Đang chạy dở")
                return
        
        combo_alpha_button = Button(nested_child, text="Combo Alpha", command=lambda: combo_alpha())
        combo_alpha_button.grid(row=0, column=4)

        def funda():
            global run_flag
            if run_flag == True:
                run_flag = False
                alpha = get_alpha()
                uni = get_universe()
                decay = get_decay()
                weight = get_weight()
                neut = get_neut()
                typ = get_type()
                delay = get_delay()
                sess.setting.set_uni(uni)
                sess.setting.set_decay(decay)
                sess.setting.set_weight(weight)
                sess.setting.set_neut(neut)
                sess.setting.set_delay(delay)
                sess.fundamental_list()
                run_flag = True
            else:
                messagebox.showerror("Error", "Đang chạy dở")
                return

        fun_button = Button(nested_child, text="Tạo bừa Fun", command=lambda: funda())
        fun_button.grid(row=0, column=5)
                           
        def cancel():
            global run_flag
            sess.stop()
            run_flag = True
            
        cancel_button = Button(nested_child, text="Hủy", command=lambda: cancel())
        cancel_button.grid(row=0, column=6)  

if __name__ == '__main__':
    # WHY THE FUCK I HAVE TO BRING THE FLAGS HERE
    run_flag = True
    
    root = Tk()
    
    root.geometry("650x480+300+300")
    root.option_add("*Font", "Arial 10")
    app = Interface(root)
    root.mainloop()
